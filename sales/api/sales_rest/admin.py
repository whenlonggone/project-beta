from django.contrib import admin
from .models import AutomobileVO, Customer, Sale

# Register your models here.
@admin.register(AutomobileVO)
class AutomobileAdmin(admin.ModelAdmin):
    list_display = (
        "vin",
        "sold",
    )

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = (
        "first_name",
        "last_name",
        "address",
        "phone_number",
    )

@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    list_display = (
        "price",
        "salesperson",
        "customer",
        "automobile",
    )
