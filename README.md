# CarCar
CarCar is an application for managing the aspects of an automobile dealership. It manages the inventory, automobile sales, and automobile services.

Team:

* Long Guan - Automobile Sales Microservice
* Shujin Pang - Automobile service microservice
​
## Getting Started
​
**Make sure you have Docker, Git, and Node.js 18.2 or above**
​
1. Fork this repository
​
2. Clone the forked repository onto your local computer:
git clone <<respository.url.here>>
​
3. Build and run the project using Docker with these commands:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- After running these commands, make sure all of your Docker containers are running
​
- View the project in the browser: http://localhost:3000/
![Img](/ghi/app/public/CarCarWebsite.png)

## Design
CarCar is made up of 3 microservices which interact with one another.
​
- **Inventory**
- **Services**
- **Sales**
![Img](/ghi/app/public/CarCarDiagram.png)

## Integration - How we put the "team" in "team"

Our Inventory and Sales domains work together with our Service domain to make everything here at **CarCar** possible.

How this all starts is at our inventory domain. We keep a record of automobiles on our lot that are available to buy. Our sales and service microservices obtain information from the inventory domain, using a **poller**, which talks to the inventory domain to keep track of which vehicles we have in our inventory so that the service and sales team always has up-to-date information.


## Accessing Endpoints to Send and View Data: Access Through Insomnia & Your Browser

### Manufacturers:


| Action | Method | URL
| ----------- | ----------- | ----------- |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/id/
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/id/
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/id/


JSON body to send data:

Create and Update a manufacturer (SEND THIS JSON BODY):
- You cannot make two manufacturers with the same name
```
{
  "name": "Chrysler"
}
```
The return value of creating, viewing, updating a single manufacturer:
```
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Chrysler"
}
```
Getting a list of manufacturers return value:
```
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  ]
}
```

### Vehicle Models:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List vehicle models | GET | http://localhost:8100/api/models/
| Create a vehicle model | POST | http://localhost:8100/api/models/
| Get a specific vehicle model | GET | http://localhost:8100/api/models/id/
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/id/
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/id/

Create and update a vehicle model (SEND THIS JSON BODY):
```
{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
  "manufacturer_id": 1
}
```

Updating a vehicle model can take the name and/or picture URL:
```
{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
}
```
Return value of creating or updating a vehicle model:
- This returns the manufacturer's information as well
```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}
```
Getting a List of Vehicle Models Return Value:
```
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "image.yourpictureurl.com",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}
```

### Automobiles:
- The **'vin'** at the end of the detail urls represents the VIN for the specific automobile you want to access. This is not an integer ID. This is a string value so you can use numbers and/or letters.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/vin/
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/vin/
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/vin/


Create an automobile (SEND THIS JSON BODY):
- You cannot make two automobiles with the same vin
```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```
Return Value of Creating an Automobile:
```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "777",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "R8",
		"picture_url": "image.yourpictureurl.com",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Audi"
		}
	}
}
```
To get the details of a specific automobile, you can query by its VIN:
example url: http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/

Return Value:
```
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "green",
  "year": 2011,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "image.yourpictureurl.com",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  }
}
```
You can update the color and/or year of an automobile (SEND THIS JSON BODY):
```
{
  "color": "red",
  "year": 2012
}
```
Getting a list of Automobile Return Value:
```
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "image.yourpictureurl.com",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      }
    }
  ]
}
```

## Service microservice

Hello and welcome to the wonderful world of service!!
As explained above, the service microservice is an extension of the dealership that looks to provide service repairs for your vehicle.

As automobiles are purchased, we keep track of the vin number of each automobile and you are able to receive the special perks of being a VIP!
As a VIP, you will receive free oil changes for life, complimentary neck massages while in our waiting room, and free car washes whenever you would like!

This area is going to be broken down into the various API endpoints (Fancy way of saying your web address url) for service along with the format needed to send data to each component.
The basics of service are as follows:
1. Our friendly technician staff
2. Service Appointments (with status as value object)

## Value Objects
The Status model provides a status to an appointment, which can be ACTIVE, CANCELED, or FINISHED.Status is a Value Object and, therefore, does not have a direct URL to view it.
AutomobileVO is also serving as a Value Object with no direct URL to view it, the data was pulled from Inventory automobile.

### Technicians - The heart of what we do here at CarCar

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/technicians/
| Create a technician | POST | http://localhost:8080/api/technicians/
| Delete a technician | DELETE | http://localhost:8080/api/technicians/<int:pk>/


LIST TECHNICIANS: Following this endpoint will give you a list of all technicians that are currently employed.
Since this is a GET request, you do not need to provide any data.
```
Example:
{
    "technicians": [
        {
            "first_name": "Jennifer",
            "last_name": "Wilson",
            "employee_id": "12345",
            "id": 1
        },
    ]
}
```

CREATE TECHNICIAN - What if we hired a new technician (In this economy even)? To create a technician, you would use the following format to input the data and you would just submit this as a POST request.
```
{
    "first_name": "George",
    "last_name": "Johnson ",
    "employee_id": "657921"
}
```
As you can see, the data has the same format. In this example, we just changed the "first_name" and "last_name"field. We also assigned him the "employee_number" value of "657921" instead of "12345".
Once we have the data into your request, we just hit "Send" and it will create the technician "George Johnson". You will receive a confirmation message saying "Technician created successfully."
To verify that it worked, just select follow the "LIST TECHNICIAN" step from above to show all technicians.With any luck, both George and Jennifer will be there.
Here is what you should see if you select "LIST TECHNICIAN" after you "CREATE TECHNICIAN" with George added in:

```
{
    "technicians": [
        {
            "first_name": "Jennifer",
            "last_name": "Wilson",
            "employee_id": "12345",
            "id": 1
        },
        {
            "first_name": "George",
            "last_name": "Johnson ",
            "employee_id": "657921",
            "id": 2
        }
    ]
}
```

DELETE TECHNICIAN - If we decide to "go another direction" as my first boss told me, then we need to remove the technician from the system. To do this, you just need to change the request type to "DELETE" instead of "POST". You also need to pull the "id" value and just change the value at the end to match the "id" of the technician you want to delete. Once they are "promoted to customer" they will no longer be in our page that lists all technicians.


And that's it! You can view all technicians,create technicians and delete technicians.
Remember, the "id" field is AUTOMATICALLY generated by the program. So you don't have to input that information. Just follow the steps in CREATE TECHNICIAN and the "id" field will be populated for you.
If you get an error, make sure your server is running and that you are feeding it in the data that it is requesting.
If you feed in the following:
```
{
    "name": "Liz",
    "employee_number": 3,
    "favorite_food": "Tacos"
}

You will get an error because the system doesn't know what what to do with "Tacos" because we aren't ever asking for that data. We can only send in data that Json is expecting or else it will get angry at us.

```

### Service Appointments: We'll keep you on the road and out of our waiting room

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List service appointments | GET | http://localhost:8080/api/appointments/
| Create service appointment | POST | http://localhost:8080/api/appointments/
| Delete service appointment | DELETE | http://localhost:8080/api/appointments/<int:id>
| Cancle an appointment      | PUT    | http://localhost:8080/api/appointments/<int:id>/cancel/
| Finish an appointment      | PUT    | http://localhost:8080/api/appointments/<int:id>/finish/


LIST SERVICE APPOINTMENT: This will return a list of all current service appointment.
This is the format that will be displayed.
Remember, the "id" is automatically generated, so you don't need to input that. Also, the "date_time" field HAVE TO BE IN THIS FORMAT
```
{
    "service_appointment": [
        {
            "href": "/api/appointments/10/",
            "reason": "battery check",
            "vin": "1234567",
            "date_time": "2023-07-25T05:35:43+00:00",
            "customer": "Courtney",
            "technician": {
                "first_name": "Jennifer",
                "last_name": "Wilson",
                "employee_id": "123",
                "id": 2
            },
            "id": 1,
            "status": "ACTIVE"
        },
    ]
}
```

CREATE SERVICE APPOINTMENT - This will create a service appointment with the data input. It must follow the format. Remember, the "id" is automatically generated, so don't fill that in. To verify
that it was added, just look at your service appointment list after creating a service appointment and it should be there. You also need to pull the "id" value of a technician to add to the "technician" filed.
you can find that value in LIST TECHNICIANS mentioned previously.the status is not necessary for creating a service appointment, it will set the status to "ACTIVE" if you don't include status information.
```
{
    "reason": "brake check",
    "vin": "1234567",
    "date_time": "2023-07-18T00:00:00+00:00",
    "customer": "Julie",
    "technician":"2"
}

```
DELETE SERVICE APPOINTMENT - Just input the "id" of the service appointment that you want to delete at the end of the url. For example, if we wanted to delete the above service history appointment for Julie
because we accidently input his name as "Judy", we would go to the service appointment list and just look at the id number for this entry. for example we see id is 2, then we just enter 'http://localhost:8080/api/serviceappointment/2' into the field and send the request. We will receive a confirmation message saying that the service appointment was deleted :" "deleted": true."


EDIT SERVICE APPOINTMENT STATUS - Just input the "id" of the service appointment that you want to edit and add either finish or cancel to the end depending on which appointment status you want. You do not need to have a JSON body for these request, and after you send the request, you will see the status changes in the response.

# Sales microservice

On the backend, the sales microservice has 4 models:
- Salesperson: A model for a sales person.
- Customer: A model for a customer.
- AutomobileVO: This is a value object obtained from the Inventory microservice using a poller. The poller polls the Inventory Microservice each second so this microservice has the updated data on the inventory of automobiles.
- Sale: All of this model's data is linked to the Salesperson model, Customer model, and AutomobileVO model except for the price field.

After every successful recording of a sale, the Sales microservice sends a PUT request to the Inventory microservice to update the sold status of an automobile. This integration follows the microservice architecture and allows for data sovereignty where data about the inventory is stored in the Inventory microservice and data about the sales is stored in the Sales microservice.

## Value Objects
AutomobileVO is a value object obtained from the Inventory microservice using a poller. This value object is used in the Sale model to associate an automobile to that sale. Since this is a value object obtained from the Inventory microservice, there are no direct ways in the Sales microservice to manipulate it.

## Accessing Endpoints to Send and View Data - Access through Insomnia:

### Customers:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List customers | GET | http://localhost:8090/api/customers/
| Create a customer | POST | http://localhost:8090/api/customers/
| Delete a customer | DELETE | http://localhost:8090/api/customers/id/

Return value of Listing all Customers:
```
{
  "customers": [
    {
      "first_name": "John",
      "last_name": "Smith",
      "address": "1800 Smith Street",
      "phone_number": "3065512268",
      "id": 1
    },
    {
      "first_name": "James",
      "last_name": "Wash",
      "address": "Example Address Street",
      "phone_number": "2065512268",
      "id": 2
    },
    {
      "first_name": "testfirst3",
      "last_name": "testlast3",
      "address": "test address washington",
      "phone_number": "8675197463",
      "id": 3
    }
  ]
}
```
To create a Customer (SEND THIS JSON BODY):
```
{
  "first_name": "John",
  "last_name": "Smith",
  "address": "1800 Smith Street",
  "phone_number": "8526734980"
}
```
Return Value of Creating a Customer:
```
{
  "first_name": "John",
  "last_name": "Smith",
  "address": "1800 Smith Street",
  "phone_number": "8526734980",
  "id": 1
}
```
Return value of Deleting a Customer:
```
{
  "deleted": true
}
```

### Salesperson:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List all salespeople | GET | http://localhost:8090/api/salespeople/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| Delete a salesperson | DELETE | http://localhost:8090/api/salespeople/id/

List all salespeople Return Value:
```
{
  "salespeople": [
    {
      "first_name": "testfirst1",
      "last_name": "testlast1",
      "employee_id": "testid1",
      "id": 1
    },
    {
      "first_name": "testfirst2",
      "last_name": "testlast2",
      "employee_id": "testid2",
      "id": 2
    },
    {
      "first_name": "testfirst3",
      "last_name": "testlast3",
      "employee_id": "testid3",
      "id": 3
    }
  ]
}
```
To create a salesperson (SEND THIS JSON BODY):
```
{
  "first_name": "John",
  "last_name": "Hello",
  "employee_id": "AFG519"
}
```
Return Value of creating a salesperson:
```
{
  "first_name": "John",
  "last_name": "Hello",
  "employee_id": "AFG519",
  "id": 1
}
```
Return Value of deleting a salesperson:
```
{
  "deleted": true
}
```

### Salesrecords:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List all sales | GET | http://localhost:8090/api/sales/
| Create a new sale | POST | http://localhost:8090/api/sales/
| Delete a sale | DELETE | http://localhost:8090/api/sales/id/

List all Salesrecords Return Value:
```
{
  "sales": [
    {
      "price": 789532,
      "id": 18,
      "salesperson": {
        "first_name": "salesperson",
        "last_name": "testlast3",
        "employee_id": "testid3"
      },
      "customer": {
        "first_name": "testfirst2",
        "last_name": "testlast2"
      },
      "automobile_vin": "523554174"
    },
    {
      "price": 59317,
      "id": 20,
      "salesperson": {
        "first_name": "testfirst2",
        "last_name": "testlast2",
        "employee_id": "testid2"
      },
      "customer": {
        "first_name": "James",
        "last_name": "Handy"
      },
      "automobile_vin": "gfdhzjipvj34571"
    }
  ]
}
```
Create a New Sale (SEND THIS JSON BODY):
- the input for salesperson is the ID of the salesperson
- the input for customer is the ID of the
- in order for a sale to be recorded, the VIN, salesperson ID, and customer ID has to be valid
```
{
  "price": "50000",
  "salesperson": "2",
  "customer": "2",
  "automobile": "3C355B2AN120174"
}
```
Return Value of Creating a New Sale:
```
{
  "price": "50000",
  "salesperson": {
    "first_name": "testfirst2",
    "last_name": "testlast2",
    "employee_id": "testid2"
  },
  "customer": {
    "first_name": "testfirst2",
    "last_name": "testlast2"
  },
  "automobile_vin": "3C355B2AN120174"
}
```
Return Value of Deleting a Sale:
```
{
  "deleted": true
}
```
