import React, {useState, useEffect} from 'react';

function AutomobileForm() {
    const [models, setModels] = useState([]);
    const [color, setColor] = useState("");
    const [year, setYear] = useState("");
    const [vin, setVin] = useState("");
    const [selectedModel, setSelectedModel] = useState("");
    const [goodSubmit, setGoodSubmit] = useState(false);

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (goodSubmit) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }

    function handleColorChange(event) {
        const { value } = event.target;
        setColor(value);
    }

    function handleYearChange(event) {
        const { value } = event.target;
        setYear(value);
    }

    function handleVinChange(event) {
        const { value } = event.target;
        setVin(value);
    }

    function handleModelChange(event) {
        const { value } = event.target;
        setSelectedModel(value);
    }

    async function getModels() {
        const url = "http://localhost:8100/api/models/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {};
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = selectedModel;

        const autoUrl = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(autoUrl, fetchConfig);
        if (response.ok) {

            setColor("");
            setYear("");
            setVin("");
            setSelectedModel("");
            setGoodSubmit(true);
        }
    }

    useEffect(() => {
        getModels();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add an automobile to inventory</h1>
                    <form className={formClasses} onSubmit={handleSubmit} id="create-automobile-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleYearChange} value={year} placeholder="Year" required type="text" name="year" id="year" className="form-control" />
                            <label htmlFor="year">Year...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} value={vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin">Vin...</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleModelChange} value={selectedModel} required name="model_id" id="model" className="form-select">
                                <option value="">Choose a model...</option>
                                {models.map(model=> {
                                    return (
                                        <option value={model.id} key={model.id}>{model.name}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                    <div className={messageClasses} id="success-message">
                        Automobile successfully added to inventory!
                    </div>
                </div>
            </div>
      </div>
    )
}

export default AutomobileForm;
