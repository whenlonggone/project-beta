import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './index.css';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import SalespersonForm from './SalespersonForm';
import SalespeopleList from './SalespeopleList';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import SalesList from './SalesList';
import SalesForm from './SalesForm';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import AppointmentHistoryList from './AppointmentHistoryList';
import SalespersonHistoryList from './SalespersonHistoryList';
import ManufacturerList from './ManufacturerList';
import ModelList from './ModelList';
import ManufacturerForm from './ManufacturerForm';
import VehicleForm from './VehicleModelForm';
import AutomobileList from './AutomobileList'
import AutomobileForm from './AutomobileForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers" element={<ManufacturerList />} />
          <Route path="/manufacturers/create" element={<ManufacturerForm />} />
          <Route path="/models" element={<ModelList/>} />
          <Route path="/technicians/create" element={<TechnicianForm />} />
          <Route path="/technicians" element={<TechnicianList />} />
          <Route path="/appointments/create" element={<AppointmentForm />} />
          <Route path="/appointments" element={<AppointmentList />} />
          <Route path="/appointments/history" element={<AppointmentHistoryList />} />
          <Route path="/salesperson/create" element={<SalespersonForm />} />
          <Route path="/salespeople" element={<SalespeopleList />} />
          <Route path="/customers/create" element={<CustomerForm />} />
          <Route path="/customers" element={<CustomerList />} />
          <Route path="/sales" element={<SalesList />} />
          <Route path="/sales/create" element={<SalesForm />} />
          <Route path="/salespersonhistory" element={<SalespersonHistoryList />} />
          <Route path="/vehicles/create" element={<VehicleForm />} />
          <Route path="/automobiles" element={<AutomobileList />} />
          <Route path="/automobiles/create" element={<AutomobileForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
