import React, {useState, useEffect} from 'react';

function SalesForm() {
    const [vins, setVins] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);

    const [vin, setVin] = useState("");
    const [salesperson, setSalesperson] = useState("");
    const [customer, setCustomer] = useState("");
    const [price, setPrice] = useState("");
    const [goodSubmit, setGoodSubmit] = useState(false);

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (goodSubmit) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }

    function handlePriceChange(event) {
        const { value } = event.target;
        setPrice(value);
    }

    function handleVinChange(event) {
        const { value } = event.target;
        setVin(value);
    }

    function handleSalespersonChange(event) {
        const { value } = event.target;
        setSalesperson(value);
    }

    function handleCustomerChange(event) {
        const { value } = event.target;
        setCustomer(value);
    }

    async function getVin() {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            let unsoldCars = [];
            for (let car of data.autos) {
                if (car.sold === false) {
                    unsoldCars.push(car);
                }
            }
            setVins(unsoldCars);
        }
    }

    async function getSalespeople() {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    }

    async function getCustomers() {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    }

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {};
        data.price = price;
        data.salesperson = salesperson;
        data.customer = customer;
        data.automobile = vin;

        const salesUrl = "http://localhost:8090/api/sales/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {

            setVin("");
            setCustomer("");
            setSalesperson("");
            setPrice("");
            setGoodSubmit(true);
        }
    }

    useEffect(() => {
        getVin();
        getSalespeople();
        getCustomers();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new sale</h1>
                    <form className={formClasses} onSubmit={handleSubmit} id="create-sales-form">
                        <div>
                            <label htmlFor="automobile">Automobile VIN</label>
                            <select onChange={handleVinChange} value={vin} required name="automobile" id="automobile" className="form-select">
                                <option value="">Choose an automobile VIN...</option>
                                {vins.map(car => {
                                    return (
                                        <option key={car.vin} value={car.vin}>{car.vin}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mt-2">
                            <label htmlFor="salesperson">Salesperson</label>
                            <select onChange={handleSalespersonChange} value={salesperson} id="salesperson" className="form-select" required>
                                <option value="">Choose a salesperson...</option>
                                {salespeople.map(person => {
                                    return (
                                        <option key={person.first_name + person.id} value={person.id}>{person.first_name + " " + person.last_name}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mt-2">
                            <label htmlFor="customer">Customer</label>
                            <select onChange={handleCustomerChange} value={customer} required name="customer" id="customer" className="form-select">
                                <option value="">Choose a customer...</option>
                                {customers.map(customer => {
                                    return (
                                        <option key={customer.first_name + customer.id} value={customer.id}>{customer.first_name + " " + customer.last_name}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mt-2 mb-3">
                            <label htmlFor="price">Price</label>
                            <input onChange={handlePriceChange} value={price} placeholder="0" required type="number" name="price" id="price" className="form-control" />
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                    <div className={messageClasses} id="success-message">
                        Sale successfully recorded!
                    </div>
                </div>
            </div>
      </div>
    )
}

export default SalesForm;
