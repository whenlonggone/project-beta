import React, { useState, useEffect } from 'react';

function SalespeopleList() {
    const [salespeople, setSalespeople] = useState([]);

    async function getSalespeople() {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    }

    useEffect(() => {
        getSalespeople();
    }, []);

    return (
        <div>
            <h1>Salespeople</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Employee ID</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map(person => {
                        return (
                            <tr key={person.employee_id + person.first_name}>
                                <td>{person.employee_id}</td>
                                <td>{person.first_name}</td>
                                <td>{person.last_name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>

    )
}

export default SalespeopleList;
