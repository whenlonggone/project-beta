import React, { useState, useEffect } from 'react';

function SalespersonHistoryList() {
    const [individualSales, setIndividualSales] = useState([]);
    const [salespersonName, setSalespersonName] = useState([]);
    const [selectedSalesperson, setSelectedSalesperson] = useState("");
    const [filteredSales, setFilteredSales] = useState([]);

    function handleSelectedSalespersonChange(event) {
        const {value} = event.target;
        setSelectedSalesperson(value);
    }

    // filter out sales to be only the sales made by selected sales person
    function filterSales() {
        let filteredSalesByPerson = [];
        for (let sale of individualSales) {
            if (selectedSalesperson === sale.salesFullName) {
                filteredSalesByPerson.push(sale);
            }
        }
        setFilteredSales(filteredSalesByPerson);
    }

    async function getSales() {
        const url = "http://localhost:8090/api/sales/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            let salesByPerson = [];
            let salespersonNameList = [];
            for (let sale of data.sales) {
                let salesFullName = sale.salesperson.first_name + " " + sale.salesperson.last_name;
                let customerFullName = sale.customer.first_name + " " + sale.customer.last_name;
                let price = sale.price;
                let vin = sale.automobile_vin;
                let individualSale = {
                    "salesFullName": salesFullName,
                    "customerFullName": customerFullName,
                    "price": price,
                    "vin": vin,
                }
                salesByPerson.push(individualSale);
                if (salespersonNameList.includes(salesFullName) === false) {
                    salespersonNameList.push(salesFullName);
                }
            }
            setIndividualSales(salesByPerson);
            setSalespersonName(salespersonNameList);
        }
    }

    useEffect(() => {
        filterSales();
    }, [selectedSalesperson])

    useEffect(() => {
        getSales();
    }, [])

    return (
        <div>
            <h1>Salesperson History</h1>
            <div>
                <select onChange={handleSelectedSalespersonChange} className="form-select">
                    <option value="">Choose a salesperson...</option>
                    {salespersonName.map(salesperson => {
                        return (
                            <option key={salesperson} value={salesperson}>{salesperson}</option>
                        );
                    })}
                </select>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Salesperson Name</th>
                        <th scope="col">Customer</th>
                        <th scope="col">VIN</th>
                        <th scope="col">Price</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredSales.map(sale => {
                        return (
                            <tr key={sale.vin}>
                                <td>{sale.salesFullName}</td>
                                <td>{sale.customerFullName}</td>
                                <td>{sale.vin}</td>
                                <td>${sale.price}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default SalespersonHistoryList;
