import React, {useState, useEffect} from 'react';

function VehicleForm() {
    const [manufacturers, setManufacturers] = useState([]);
    const [name, setName] = useState("");
    const [pictureUrl, setPictureUrl] = useState("");
    const [selectedManu, setSelectedManu] = useState("");
    const [goodSubmit, setGoodSubmit] = useState(false);

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (goodSubmit) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }

    async function getManufacturers() {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    function handleManufacturerChange(event) {
        const { value } = event.target;
        setSelectedManu(value);
    }

    function handleNameChange(event) {
        const { value } = event.target;
        setName(value);
    }

    function handlePictureChange(event) {
        const { value } = event.target;
        setPictureUrl(value);
    }

    useEffect(() => {
        getManufacturers();
    }, []);

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.picture_url = pictureUrl;
        data.manufacturer_id = selectedManu;

        const vehicleUrl = "http://localhost:8100/api/models/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(vehicleUrl, fetchConfig);
        if (response.ok) {
            setName("");
            setPictureUrl("");
            setSelectedManu("");
            setGoodSubmit(true);
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a vehicle model</h1>
                    <form className={formClasses} onSubmit={handleSubmit} id="create-vehicle-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} placeholder="Model name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Model name...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureChange} value={pictureUrl} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture URL...</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleManufacturerChange} value={selectedManu} required name="manufacturers" id="manufacturers" className="form-select">
                                <option value="">Choose a manufacturer...</option>
                                {manufacturers.map(manufacturer=> {
                                    return (
                                        <option value={manufacturer.id} key={manufacturer.id}>{manufacturer.name}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                    <div className={messageClasses} id="success-message">
                        Vehicle model successfully added!
                    </div>
                </div>
            </div>
      </div>
    )
}

export default VehicleForm;
