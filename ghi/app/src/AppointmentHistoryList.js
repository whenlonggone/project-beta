import React, { useState, useEffect } from 'react';

function AppointmentHistoryList() {
    const [appointments, setAppointments] = useState([]);
    const [automobileVins, setAutomobileVins] = useState([]);
    const [vinInput, setVinInput] = useState('');

    const handleVinInputChange = (event) => {
        setVinInput(event.target.value);
      };

    function filterAppointments(vin) {
        const filteredAppointments = appointments.filter(
          (appointment) => appointment.vin === vin
        );
        setAppointments(filteredAppointments);
      }

    const handleVinSubmit = (event) => {
        event.preventDefault();
        filterAppointments(vinInput);
      };


    async function getAppointments() {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    }

    useEffect(() => {
        getAppointments();
    }, []);


    function formatDateAndTime(dateTime) {
        const formattedDate = new Date(dateTime).toLocaleDateString();
        const formattedTime = new Date(dateTime).toLocaleTimeString();
        return { formattedDate, formattedTime };
    }

    return (
            <div>
                <h1>Service Appointments</h1>
                <div className="d-flex align-items-center">
                    <form onSubmit={handleVinSubmit} className="d-flex">
                    <input type="text" value={vinInput} onChange={handleVinInputChange} placeholder="Search by VIN..." name="vin" id="vin" className="form-control me-2" style={{ width: '1000px' }}/>
                    <button type="submit"  className="btn btn-success">
                        Search
                    </button>
                    </form>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">VIN</th>
                        <th scope="col">is VIP</th>
                        <th scope="col">Customer</th>
                        <th scope="col">Data</th>
                        <th scope="col">Time</th>
                        <th scope="col">Technician</th>
                        <th scope="col">Reason</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        const { formattedDate, formattedTime } = formatDateAndTime(appointment.date_time);
                        const isVIP = automobileVins.includes(appointment.vin) ? "Yes" : "No";
                        return (
                            <tr key={appointment.href}>
                                <td>{appointment.vin}</td>
                                <td>{isVIP}</td>
                                <td>{appointment.customer}</td>
                                <td>{formattedDate}</td>
                                <td>{formattedTime}</td>
                                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.status}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>

    )
}

export default AppointmentHistoryList;
